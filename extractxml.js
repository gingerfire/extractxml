var fs = require('fs');
var util = require('util');
var sax = require('sax');
var xmlbuilder = require('xmlbuilder');
var dom = [];

var cla = require('command-line-args');
var args = cla([
    { name: 'expression', alias: 'e', multiple: true, type: String },
    { name: 'in', alias: 'i', multiple: false, type: String },
    { name: 'out', alias: 'o', multiple: false, type: String }
]);

var nodeList = [];
var attributeList = [];

var nodeOnly = /^([^:]+)$/;
var nodeAttr = /^([^:]+):([^=]+)$/;
var nodeAttrValue = /^([^:]+):?([^=]+)=?(.+)$/;

var nodePath = [];
var nodeIndex = [];

function isSpace(text) {
    return /^\s+$/.test(text);
}

function getRegExp(str) {
    if (/^\//.test(str) && /\/$/.test(str)) {
        return new RegExp(str.replace(/^\//, '').replace(/\/$/, ''));
    } else {
        return new RegExp('^' + str + '$');
    }
}

function str2obj(str, delim, prefix) {
    var output = {};
    var keyPairs = [];

    if (!str) {
        str = '';
    }
    if (delim == undefined) {
        delim = '&';
    }
    if (prefix == undefined) {
        prefix = '';
    }

    if (str != '') {
        keyPairs = str.split(delim);

        if (keyPairs.length > 0) {
            output = {};

            keyPairs.forEach(function (kp) {
                var tokens = kp.split('=');
                var key = '';

                if (tokens.length > 0) {
                    key = prefix + tokens[0]
                    output[key] = '';
                }

                if (tokens.length > 1) {
                    output[key] = tokens[1];
                }
            });
        }
    }

    return output;
}

function test(nodeName, attributes) {
    var nodeMatch = false;
    var attrMatch = null;
    var valueMatch = null;
    var result = false;

	for (var i = 0; i < attributeList.length; i++) {
		var test = attributeList[i];
        nodeMatch = test.node.test(nodeName);

        if (nodeMatch && attributes) {
            for (var a in attributes) {
                if (!attrMatch && test.attribute) {
                    attrMatch = test.attribute.test(a);
                }

                if (!valueMatch && test.value) {
                    valueMatch = test.value.test(attributes[a]);
                }
            }
        }

        result = nodeMatch;

        if (attrMatch != null) {
            result = result && attrMatch;
        }

        if (valueMatch != null) {
            result = result && valueMatch;
        }

		if (result) {
			return true;
		}
    }

    return false;
}

function nodeMatch(n1, n2) {
    function intersect(array1, array2) {
       var result = [];
       // Don't destroy the original arrays
       var a = array1.slice(0);
       var b = array2.slice(0);
       var aLast = a.length - 1;
       var bLast = b.length - 1;
       while (aLast >= 0 && bLast >= 0) {
          if (a[aLast] > b[bLast] ) {
             a.pop();
             aLast--;
          } else if (a[aLast] < b[bLast] ){
             b.pop();
             bLast--;
          } else /* they're equal */ {
             result.push(a.pop());
             b.pop();
             aLast--;
             bLast--;
          }
       }
       return result;
    }

    var match = true;

    var k1 = Object.keys(n1).filter(function (v) { return (v.indexOf('@') == 0) });
    var k2 = Object.keys(n2).filter(function (v) { return (v.indexOf('@') == 0) });;
    var i = intersect(k1, k2);

    match = (k1.length == k2.length && k2.length == i.length);

    if (match) {
        i.forEach(function (k) {
            if (/^\@/.test(k) && (k in n2)) {
                match = match && n1[k] == n2[k];
            }
        });
    }

    return match;
}

function build(nodes) {
    var obj = {};
    var currNode;
    var path = /^([^:]+):::(.*)$/;
    var pathLast = 0;

    if (nodes.length == 0) {
        return;
    }

    nodes.forEach(function (node) {
        currNode = obj;

        // Find the correct node placement
        pathLast = node.path.length - 1;

        node.path.forEach(function (p, i) {
            var tokens = path.exec(p);
            var nodeName = tokens[1];
            var attrs = (tokens.length > 2 ? str2obj(tokens[2], ';;;', '@') : {});

            if (i >= pathLast && 'text' in node) {
                attrs['#text'] = node.text;
            }

            if (!(nodeName in currNode)) {
                currNode[nodeName] = attrs;
                currNode = currNode[nodeName];
            } else {
                if (!util.isArray(currNode[nodeName])) {
                    if (nodeMatch(currNode[nodeName], attrs)) {
                        if (i < pathLast) {
                            currNode = currNode[nodeName];
                        }
                    } else {
                        currNode[nodeName] = [currNode[nodeName]];
                        currNode[nodeName].push(attrs);
                        currNode = currNode[nodeName][currNode[nodeName].length -1];
                    }
                } else {
                    var target = null;

                    for (var j = 0; j < currNode[nodeName].length; j++) {
                        if (nodeMatch(currNode[nodeName][j], attrs)) {
                            target = currNode[nodeName][j];
                        }
                    }

                    if (target) {
                        currNode = target;
                    } else {
                        currNode[nodeName].push(attrs);
                        currNode = currNode[nodeName][currNode[nodeName].length -1];
                    }
                }
            }
        });
    });

    var xml = xmlbuilder.create(obj, {
        version: '1.0',
        encoding: 'UTF-8'
    });

    var out = process.stdout;

    if (args.out) {
        out = fs.createWriteStream(args.out);
    }

    var writer = xmlbuilder.streamWriter(out, { pretty: true });
    xml.end(writer);
}

function addExpressions(exprs) {
	if (!Array.isArray(exprs) && (typeof exprs == 'string')) {
		exprs = [exprs];
	}
    exprs.forEach (function (v) {
        var tokens = [];
        var attribute = {
            node: null,
            attribute: null,
            value: null
        }

        if (nodeOnly.test(v)) {
            tokens = nodeOnly.exec(v);
        } else if (nodeAttr.test(v)) {
            tokens = nodeAttr.exec(v);
            attribute.attribute = getRegExp(tokens[2]);
        } else if (nodeAttrValue.test(v)) {
            tokens = nodeAttrValue.exec(v);
            attribute.attribute = getRegExp(tokens[2]);
            attribute.value = getRegExp(tokens[3]);
        }

        attribute.node = getRegExp(tokens[1]);
        attributeList.push(attribute);

        if (nodeList.indexOf(tokens[1]) == -1) {
            nodeList.push(tokens[1]);
        }
    });
}

if (('expression' in args) && ('in' in args)) {
	addExpressions(args.expression);

    var saxStream = sax.createStream(true);
    saxStream.on('error', function (e) {
        throw e;
    });
    saxStream.on('opentag', function (node) {
        var attributes = [];

        if ('attributes' in node) {
            for (var p in node.attributes) {
                attributes.push(p + '=' + node.attributes[p]);
            }
        }

        nodePath.push(node.name + ':::' + attributes.join(';;;'));

		if (node.name == 'shared-option') {
			addExpressions('product-option:option-id=' + node.attributes['option-id']);
		} else if (node.name == 'shared-variation-attribute') {
			addExpressions('variation-attribute:attribute-id=' + node.attributes['attribute-id']);
		}
    });
    saxStream.on('closetag', function (name) {
        nodePath.pop();
    });
    saxStream.on('text', function (text) {
        if (!isSpace(text)) {
            nodeIndex.push({
                path: Array.prototype.slice.call(nodePath),
                text: text
            });
        }
    });
    saxStream.on('opencdata', function () {

    });
    saxStream.on('cdata', function (cdata) {
        if (!isSpace(text)) {
            nodeIndex.push({
                path: Array.prototype.slice.call(nodePath),
                text: cdata
            });
        }
    });
    saxStream.on('closecdata', function () {

    });
    saxStream.on('end', function () {
        nodeIndex = nodeIndex.filter(function (n) {
            var result = false;

            if ('path' in n) {
                n.path.forEach(function (p) {
                    var tokens = p.split(':::');
                    var nodeName = tokens[0];
                    var attributes = (tokens.length > 1 ? str2obj(tokens[1], ';;;') : null);

                    result = result || test(nodeName, attributes);
                });
            }

            return result;
        });

		//console.log(nodeIndex);
        build(nodeIndex);
    });

    fs.createReadStream(args.in)
    .pipe(saxStream)
}
