# ExtractXML.js

This Node.js script extracts XML nodes from a source XML file.

In order to maintain the integrity of the XML DOM, matched nodes also extract their ancestral line. For example, given the following book in XML format:

    <book>
        <prologue>Curabitur ac sollicitudin metus, sit amet maximus erat.</prologue>
        <chapter chapter="1">
    	    <title>Chapter 1</title>
    		<text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</text>
    	</chapter>
    	<chapter chapter="2">
    	    <title>Chapter 2</title>
    		<text>Maecenas cursus mi sit amet euismod facilisis.</text>
    	</chapter>
        <chapter chapter="A">
    	    <title>Appendix</title>
    		<text>Praesent suscipit nunc urna, et suscipit urna blandit non.</text>
    	</chapter>
        <epilogue>Phasellus pharetra a ex in eleifend. Aenean tristique viverra massa a semper.</epilogue>
    </book>

you could extract the chapter titles with this command line:

    node extractxml.js -e "title" -i book.xml

resulting in the following XML:

    <book>
	    <chapter chapter="1">
            <title>Chapter 1</title>
        </chapter>
        <chapter chapter="2">
            <title>Chapter 2</title>
        </chapter>
    </book>



## Getting Started

Download the ExtractXML.js repository:

    git clone https://bitbucket.org/gingerfire/extractxml.git

Install the latest version of [Node.js](https://node.js.org/) for your operating system. The Node.js package should include the `npm` package manager.

In the extractxml folder, install dependencies by running this command:

    npm install



## Command-line Arguments

extractxml.js takes the following arguments:

    -e <expression>
    -i <input>
    -o <output>



### Expressions

ExtractXML.js provides a flexible expression syntax for matching XML nodes. The general syntax is like this:

    node:attribute=value

Each portion of the expression (node, attribute, value) can be a regular expression, denoted by the forward-slash ("/") delimiters. For example:

    node extractxml.js -e "/logue$/" -i examples/book.xml

yields:

    <book>
        <prologue>Curabitur ac sollicitudin metus, sit amet maximus erat.</prologue>
        <epilogue>Phasellus pharetra a ex in eleifend. Aenean tristique viverra massa a semper.</epilogue>
    </book>

and

    node extractxml.js -e "chapter:chapter=/[0-9]+/" -i examples/book.xml

yields:

    <book>
        <chapter chapter="1">
            <title>Chapter 1</title>
            <text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</text>
        </chapter>
        <chapter chapter="2">
            <title>Chapter 2</title>
            <text>Maecenas cursus mi sit amet euismod facilisis.</text>
        </chapter>
    </book>

### Input

The input file is specified using the `-i` switch. Currently, only filesystem files can be provided as input (i.e. no `stdin` streaming).

### Output

If the `-o` switch is present with a file name, the resulting XML will be written to the specified file. If the `-o` switch is not present, output will be written to `stdout`.



## Caveats

The nodes are parsed by the uniqueness of the ancestral path and the node attributes along the way. For example, the following XML:

    <book>
        <chapter>
            <title>Chapter 1</title>
            <text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</text>
        </chapter>
        <chapter>
            <title>Chapter 2</title>
            <text>Maecenas cursus mi sit amet euismod facilisis.</text>
        </chapter>
    </book>

with the following command-line:

    node extractxml.js -e "chapter" -i examples/book2.xml

yields:

    <book>
      <chapter>
        <title>Chapter 1</title>
        <text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</text>
      </chapter>
    </book>

which is obvioiusly not correct (it should have extracted all of the `chapter` nodes). This extractor may be reworked in the future to allow similar node/attribute ancestries to be distinguishable.
